﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using main_app.Models;
using main_app.Filters;

namespace main_app
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.Configure<AppConfig>(Configuration);

            services.AddMvc(options => {
                options.Filters.Add(new UtmCaptureFilter());

				if (Configuration.GetSection("Config")["authGate"].ToLower() == "on")
				{
					options.Filters.Add(new AuthorizeFilter(new AuthorizationPolicyBuilder().RequireRole("user").Build()));
				}
			});

            services.AddDistributedMemoryCache();
            services.AddSession();
            
            var config = new AppConfig();
            Configuration.Bind(config);

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

			services.Configure<SiteConfig>(Configuration.GetSection("ExternalUrls"));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.Use(async(context, next) =>
            {
                await next();
                if (context.Response.StatusCode == 404) {
                    context.Response.Redirect("/Error/NotFound404");
                }
            });

            app.UseStaticFiles();
            app.UseSession();

			app.UseCookieAuthentication(new CookieAuthenticationOptions()
            {
                AuthenticationScheme = "Cookie",
                LoginPath = new PathString("/Account/Login"),
                AccessDeniedPath = new PathString("/Account/Login/"),
                AutomaticAuthenticate = true,
                AutomaticChallenge = true,
                CookieSecure = CookieSecurePolicy.SameAsRequest
            });

            app.UseMvc(routes =>
            {
                var enabledSite = Configuration["Config:EnabledSite"];
                var supportedSiteConfigurations = new string[] {
                    "Patient",
                    "HCP"
                };

                if (!supportedSiteConfigurations.Contains(enabledSite))
                {
                    throw new Exception($"Unknown EnabledSite configuration: {enabledSite}");
                }

                if (enabledSite == "HCP")
                {
                    routes.MapRoute(
                        "404 error",
                        "error/notfound404",
                        defaults: new { controller = "hcp", action = "error" }
                    );

                   routes.MapRoute(
                        name: "default",
                        template: "{action=Index}/{id?}",
                        defaults: new { controller = "hcp" }
                    );
                } else {
                    routes.MapRoute(
                        "404 error",
                        "error/notfound404",
                        defaults: new { controller = "home", action = "error" }
                    );

                   routes.MapRoute(
                        name: "default",
                        template: "{action=Index}/{id?}",
                        defaults: new { controller = "home" }
                    );
                }

                routes.MapRoute(
                    name: "auth",
                    template: "{controller=Account}/{action=Login}/{id?}"
                    );

            });
        }
    }
}
