﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using main_app.Models;
using System.Reflection;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System.Collections.Generic;

namespace main_app.ValidationAttributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class RequireFromGroupAttribute : ValidationAttribute, IClientModelValidator
    {
        public string Selector { get; }

        public bool IncludeOthersFieldName { get; set; }

        public RequireFromGroupAttribute(string selector): base("Please fill at least 1 of these fields")
        {
            this.Selector = selector;
            this.IncludeOthersFieldName = true;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var properties = this.GetInvolvedProperties(validationContext.ObjectType); ;

            var values = new List<object> { value };
            var otherPropertiesValues = properties.Where(p => p.Key.Name != validationContext.MemberName)
                                                  .Select(p => p.Key.GetValue(validationContext.ObjectInstance));
            values.AddRange(otherPropertiesValues);

            if (values.Count(s => !string.IsNullOrWhiteSpace(Convert.ToString(s))) >= 1)
            {
                return ValidationResult.Success;
            }

            return new ValidationResult(this.GetErrorMessage(properties.Values), new List<string> { validationContext.MemberName });
        }

        private Dictionary<PropertyInfo, string> GetInvolvedProperties(Type type)
        {
            return type.GetProperties()
                       .Where(p => p.IsDefined(typeof(RequireFromGroupAttribute)) &&
                                   p.GetCustomAttribute<RequireFromGroupAttribute>().Selector == this.Selector)
                       .ToDictionary(p => p, p => p.IsDefined(typeof(DisplayAttribute)) ? p.GetCustomAttribute<DisplayAttribute>().Name : p.Name);
        }

        private string GetErrorMessage(IEnumerable<string> properties)
        {
            var errorMessage = this.ErrorMessageString;
            if (this.IncludeOthersFieldName)
            {
                errorMessage += ": " + string.Join(", ", properties);
            }

            return errorMessage;
        }

        public void AddValidation(ClientModelValidationContext context)
        {
            var properties = this.GetInvolvedProperties(context.ModelMetadata.ContainerType);
            context.Attributes["data-val"] = "true";
            context.Attributes["data-val-requirefromgroup"] = this.GetErrorMessage(properties.Values);
            context.Attributes["data-val-requirefromgroup-selector"] = this.Selector;
        }
    }
}
