using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace main_app.ValidationAttributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class RequireIfTrueValidationAttribute : ValidationAttribute, IClientModelValidator
    {
        public string DependentProperty { get; private set; }

        public RequireIfTrueValidationAttribute(string dependentProperty)
        {
            DependentProperty = dependentProperty;
        }

        private bool GetDependentPropertyValue(object container)
        {
            var currentType = container.GetType();
            var property = currentType.GetProperty(DependentProperty);

            var value = property.GetValue(container);

            return (bool)value;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var propValue = GetDependentPropertyValue(validationContext.ObjectInstance);
            if (propValue)
            {
                var required = new RequiredAttribute();
                var val = required.IsValid(value);
                if (!val)
                    return new ValidationResult(this.ErrorMessageString);
            }
            return ValidationResult.Success;
        }

        public void AddValidation(ClientModelValidationContext context)
        {
            context.Attributes["data-val"] = "true";
            context.Attributes["data-val-requireiftrue"] = this.ErrorMessageString;
            context.Attributes["data-val-requireiftrue-dependentproperty"] = this.DependentProperty;
        }
    }
}