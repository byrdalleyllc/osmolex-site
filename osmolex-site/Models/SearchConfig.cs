﻿namespace main_app.Models {
	public class ConfigSearch
	{
		public string SRCH_HOST { get; set; }
		public string SRCH_USER { get; set; }
		public string SRCH_PASSWORD { get; set; }
		public string SRCH_COLLECTION { get; set; }
	}
}