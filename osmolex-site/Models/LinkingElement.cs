﻿using System;
namespace main_app
{
	public class LinkingElement
	{
		public string url { get; set; }
		public string linkType { get; set; }

		public string GetUrlTarget()
		{
			return this.url.Contains(".pdf") ? "_blank" : string.Empty;
		}
	}
}
