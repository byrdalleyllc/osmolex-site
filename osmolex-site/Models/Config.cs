namespace main_app.Models
{
    public class AppConfig
    {
        public AuthConfig Config {get;set;}
    }

    public class SiteConfig
    {
        public string hcpUrl { get; set; }
        public string patientUrl { get; set; }
    }

    public class AuthConfig
    {
        public string authGate {get;set;}
		public string authUsername {get;set;}
		public string authPassword {get;set;}
        public string EnabledSite { get; set; }
    }
}
