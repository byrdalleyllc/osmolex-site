﻿using System;
namespace main_app
{
	public class CTA : LinkingElement
	{
		public string heading { get; set; }
		public string description { get; set; }
		public string action { get; set; }
		public string image { get; set; }
		public string styleType { get; set; }
		public string altTag { get; set; }
		public string gaTag {get; set;}

		public CTA(string heading, string description, string image, string url, string action, string styleType, string altTag, string gaTag, string linkType = null )
		{
			this.heading = heading;
			this.description = description;
			this.image = image;
			this.url = url;
			this.action = action;
			this.linkType = linkType;
			this.styleType = styleType;
			this.altTag = altTag;
			this.gaTag = gaTag;

		}
	}
}
