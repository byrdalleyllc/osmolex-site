namespace main_app.Models
{
    public class SearchResult
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Controller { get; set; }
        public string View { get; set; }
    }
}