module.exports.tasks = {
	copy: {
		dev:  {
			files: [
				{ src: [ '<%= paths.fonts %>/**' ], dest: '<%= paths.dist %>/' },
				{ src: [ '<%= paths.img %>/**' ], dest: '<%= paths.dist %>/' },
                { src: [ '<%= paths.misc %>/**' ], dest: '<%= paths.dist %>/' },
			]
		},
		dist: {
			files: [
				{
					expand: true,
					src:    '**/*',
					dest:   '<%= paths.dist %>/<%= paths.css %>/',
					cwd:    '<%= paths.css %>'
				}, {
					src:  [ '<%= paths.fonts %>/**' ],
					dest: '<%= paths.dist %>/'
				}
			]
		}
	}
};
