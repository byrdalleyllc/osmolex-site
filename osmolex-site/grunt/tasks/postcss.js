var pixrem        = require( 'pixrem' )(),                  // REM / pixel fallbacks
    cssnano       = require( 'cssnano' )(),                 // File min / optimizations
    mqpacker      = require( 'css-mqpacker' )(),            // Combines multiple of the same media queries into a single condition
    browserTarget = '> 10%, last 2 versions, ie >= 10, Safari 8';

// Check against caniuse db
var doiuse = require( 'doiuse' )( {
	ignore:         [ 'rem' ],
	browsers:       browserTarget,
	onFeatureUsage: function( usageInfo ){
		
		var re  = /(.+):\s(.+):\s(.+)\((.+)\)/;
		var str = usageInfo.message,
		    m;
		
		if( (m = re.exec( str )) !== null ){
			if( m.index === re.lastIndex ){
				re.lastIndex++;
			}
		}
	}
} );

// vendor auto-prefixing
var autoprefixer = require( 'autoprefixer' )( {
	atrules:  true,
	browsers: browserTarget
} );

// the actual fiels object
var targetPath = '<%= paths.dist %>/<%= paths.css %>/';
var filesGlob = [
	{
		expand: true,
		cwd:    targetPath,
		src:    [ '**/*.css' ],
		dest:   targetPath,
	}
];

module.exports.tasks = {
	postcss: {
		dist:    {
			files: filesGlob,
			options: {
				processors: [ pixrem, doiuse, autoprefixer, mqpacker, cssnano ],
				map: {
					inline:     false,
					annotation: targetPath
				}
			}
		},
		dev:     {
			files: filesGlob,
			options: {
				processors: [ pixrem, autoprefixer, mqpacker ]
			}
		}
	}
};