module.exports.tasks = {
  uglify: {
    dist: {
      files: [{
        expand: true,
        src: '**/*.js',
        dest: '<%= paths.dist %>/<%= paths.js %>',
        cwd: '<%= paths.js %>',
        ext: '.min.js'
      }]
    },
    options: {
      sourceMap: true,
      mangle: true,
      preserveComments: false
    }
  }
};