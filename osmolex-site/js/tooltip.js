$('[title]').hover(
    function() {
        $(this).data('title', $(this).attr('title')).attr('title',null);
    },
    function() {
        $(this).attr('title', $(this).data('title'));
    }
);