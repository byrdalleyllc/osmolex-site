$('.transcript-trigger').click(function(e) {
    var t = $(e.target);
    t.closest('.transcript-container').find('.transcript').toggle();
    t.toggleClass('inactive');
})