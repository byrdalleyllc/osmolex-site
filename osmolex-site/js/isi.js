var $isiDrawer = $('#isi-drawer');
var $isiFixed = $('#isi-fixed');
var $isiToggleUp = $('#isi-toggle-up-button');
var $isiFocusModal = $('#isi-modal');
var $isiFocusBody = $('#isi-focus-body');
var $isiModalHeader = $('#isi-modal-header');
var $isiMainNav = $('.isiMainNav');
var lastScrollTop = 0;

toggleDrawer(false);
$(window).on('scroll touchmove mousewheel', function (e) {
    var st = $(this).scrollTop();
    if (st > lastScrollTop) {
       toggleDrawer(false);
    } else if (st < lastScrollTop) {
        toggleDrawer(true);
    }
    lastScrollTop = st;
});
function toggleDrawer(up) {
    if (up) {
        //if we are scrolling up show drawer if position is correct
        $isiDrawer.show();
        if ($isiDrawer.offset().top < $isiFixed.offset().top)
            $isiDrawer.show();
        else
            $isiDrawer.hide();
    }
    else {
        if ($isiDrawer.offset().top > $isiFixed.offset().top)
            $isiDrawer.hide();
    }
}
$isiToggleUp.on('click', function (e) {
    var $htmlContent = $isiFixed.children().clone();
    var $modalHeader = $htmlContent.clone().slice(0, 1);
    var $modalContent = $htmlContent.slice(1);
    $isiFocusBody.replaceWith($modalContent);
    $isiModalHeader.replaceWith($modalHeader);
    $isiFocusModal.modal({
        // backdrop: 'static',
        keyboard: false
    });
    initIsiOffset();
});

$isiMainNav.on('click', function (e) {
    e.preventDefault();
    var $htmlContent = $isiFixed.children().clone();
    var $modalHeader = $htmlContent.clone().slice(0, 1);
    var $modalContent = $htmlContent.slice(1);
    $isiFocusBody.replaceWith($modalContent);
    $isiModalHeader.replaceWith($modalHeader);
    $isiFocusModal.modal({
        // backdrop: 'static',
        keyboard: false
    });
    initIsiOffset();
});

function initIsiOffset() {
    $isiFocusModal.css({ top: $('.layout--main-nav').offset().top + $('.layout--main-nav').outerHeight() });
}

$isiFocusModal.on('show.bs.modal', function (e) {
    $isiDrawer.hide();
    $('.reference').hide();

    setTimeout(function() {
        $('.modal-backdrop').on('click', function() {
            $('#isi-modal').modal('hide');
        });
    }, 500);
});

$isiFocusModal.on('hide.bs.modal', function (e) {
    $isiDrawer.show();
    $('.reference').show();
});