var sticky = {
    stickies: null,
    initialize: function() {
        this.initializeElements();

        this.attach();

        $(window).on('menuInitialized', $.proxy(this.reset, this));

        $(window).on('showNav', $.proxy(this.detach, this));
        $(window).on('hideNav', $.proxy(this.attach, this));
    },
    reset: function() {
        this.initializeElements();

        this.detach().done($.proxy(this.attach, this));
    },
    initializeElements: function() {
        $('#nav-container').trigger('sticky_kit:detach').stick_in_parent();

        var options = {offset_top: $('#nav-container').height()},
            notMobile = function() {
                return $(window).width() >= vars.mobile.width;
            };

        this.stickies = [
            {
                id: 'nav-container',
                options: null
            },
            {
                id: 'page-nav-container',
                options: options,
                condition: notMobile
            },
            {
                id: 'featured-content',
                options: _.extend({}, options, {parent: '#featured-content-container'}),
                condition: notMobile
            }
        ];
    },
    attach: function() {
        $(this.stickies).each(function(i, sticky) {
            if ($('#' + sticky.id).length > 0 && (sticky.condition == null || sticky.condition())) {
                $('#' + sticky.id).trigger('sticky_kit:detach').css('position', 'static').stick_in_parent(sticky.options);
            }
        });
    },
    detach: function() {
        $(this.stickies).each(function(i, sticky) {
            $('#' + sticky.id).trigger('sticky_kit:detach');
        });

        var dfd = new $.Deferred();
        setTimeout(function() {dfd.resolve()}, 1);

        return dfd;
    }
}

$(document).ready(function() {
    sticky.initialize();
});

var banner = {
    initialize: function() {
        this.initializeClose();
    },
    initializeClose: function() {
        $('.banner .close').click(function() {
            var banner = $(this).closest('.banner');

            banner.remove();

            var type = banner.hasClass('layout--signup-cta') ? 1 : 0;

            $.ajax('/settings/setbannerdismissed?type=' + type);
        });
    }
}

banner.initialize();