function showImage(link,note) {
    var i = $('#expandable-image');

    i.on('show.bs.modal', function(e) {
        var m = $(this);
        m.find('.modal-main-image').attr('src', link);
        if(note!=""){
            m.find('.modal-body').append( "<p>"+note+"</p>" );
        }
    });

    i.modal();
}

$(document).ready(function() {
    $('[data-expandable=1]').click(function(e) {
        e.preventDefault();

        var target = $(e.target).eq(0);
        if (!target.is('img')) {
            note = target.closest('[data-expandable=1]').find('.note');
            target = target.closest('[data-expandable=1]').find('img');
        }

        var link = target.attr('src');
        var noteText = "";
        if (note){
            noteText = note.text();
        }
        showImage(link,noteText);
    });

    $('#expandable-image .btn-primary').click(function() {
        $('#expandable-image').modal('hide');
    });
});