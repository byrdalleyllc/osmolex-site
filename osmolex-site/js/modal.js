function showInterstitial(title, body, link) {
    var i = $('#modal');

    i.on('show.bs.modal', function(e) {
        var m = $(this);
        m.find('.modal-title').text(title);
        m.find('.modal-body').text(body);
       // m.find('.action-hcp').attr('target', '_blank').attr('href', link).click();
    });

    i.modal();
}

function goInternal(link) {
    var title = 'Are you a US healthcare professional?',
        body = 'The information on OSMOLEX.com/hcp is different than the information found in the patient section and is intended for US healthcare professionals.';

    showInterstitial(title, body, link);
}

$(document).ready(function() {
    $('[data-toggle=modal]').click(function(e) {
        e.preventDefault();

        var target = $(e.target);
        if (!target.is('a')) {
            target = target.closest('a');
        }

        var link = target.attr('href'),
            type = target.attr('data-link');

            goInternal(link);
    });

    $('#modal .btn-default').click(function() {
        $('#modal').modal('hide');
    });
});