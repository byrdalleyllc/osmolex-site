var defaultRangeValidator = $.validator.methods.range;
$.validator.methods.range = function (value, element, param) {
    if (element.type === 'checkbox' || element.type === 'radio') {
        // if it's a checkbox return true if it is checked
        return element.checked;
    } else {
        // otherwise run the default validation function
        return defaultRangeValidator.call(this, value, element, param);
    }
};
$.validator.addMethod("require_if_true", function (value, element, options) {
    if (value == '')
        return false;
    return true;
});
$.validator.unobtrusive.adapters.add("requireiftrue", ["dependentProperty"], function (options) {
    options.rules["require_if_true"] = options.params.dependentProperty;
    options.messages["require_if_true"] = options.message;
});