// Require any common libraries here.
require('jquery');
require('jquery-validate');
require('jquery-validate-unobtrusive');
require( 'underscore' );
require( 'bootstrap' );
require( 'moment' );
require( 'bowser' );
require( 'shufflejs' );
require( 'sticky-kit' );
require( 'owl.carousel' );
require( 'tippy' );
