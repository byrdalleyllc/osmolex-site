﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace main_app.Controllers
{
    public class HcpController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

		public IActionResult About()
        {
            return View();
        }

		public IActionResult Dosing()
		{
			return View();
		}

		public IActionResult Affordability()
		{
			return View();
		}

		public IActionResult Resources()
		{
			return View();
		}

		public IActionResult Error()
        {
            return View();
        }

		public IActionResult SiteMap()
        {
            return View();
        }
    }
}
