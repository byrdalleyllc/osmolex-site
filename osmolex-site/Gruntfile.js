﻿/*global module:false*/
module.exports = function (grunt) {

    // Read in project specific config file (specifies
    // path locations, output filenames, ports to run on, etc.)
    var gruntfileConfig = grunt.file.readJSON('grunt/gruntfileConfig.json');

    // Load all grunt tasks into memory
    require('load-grunt-tasks')(grunt);

    // Config project paths & references
    var options = {
        pkg: grunt.file.readJSON('package.json'),  // npm packages file
        gruntfileConfig: gruntfileConfig,                      // project's grunt config file
        paths: gruntfileConfig.paths,                // setup specified project paths from grunt config file,
        files: gruntfileConfig.files,                // setup specified project paths from grunt config file,
        config: {
            src: "grunt/tasks/*.js"                    // path to where the grunt tasks reside
        }
    };

    // Load the various task configuration files with options from above
    var configs = require('load-grunt-configs')(grunt, options);

    // Initialize Grunt with configs
    grunt.initConfig(configs);

    grunt.registerTask('build:dev', "Build the site and package up it's components without minify or uglify", [
        'clean',
        'sass',
        'postcss:dev',
        'browserify',
        'copy:dev',
    ]);

    grunt.registerTask('build:prod', "Build the site and package up it's components", [
        'clean',
        'sass',
        'postcss:dist',
        'browserify',
        'copy:dist',
        'uglify',
//        'imagemin'
    ]);
};
